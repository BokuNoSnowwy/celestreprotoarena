﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameCollider : MonoBehaviour
{
    public GameObject endGamePanel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DisplayEndGamePanel()
    {
        if (!endGamePanel.activeSelf)
        {
            endGamePanel.SetActive(true);
        }
        else
        {
            endGamePanel.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            DisplayEndGamePanel();
        }
    }
}
