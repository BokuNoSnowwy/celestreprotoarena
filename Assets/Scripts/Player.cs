﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Board actualBoard;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Die();
        }
    }

    public void Die()
    {
        Debug.Log("Respawn");
        actualBoard.RespawnPlayerHere();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Board"))
        {
            //Swap cam 
            Board board =  other.GetComponentInParent<Board>();
            if (board != actualBoard)
            {
                if (actualBoard.GetComponent<Board>().containBadeline)
                {
                    foreach (GameObject badeline in actualBoard.GetComponent<Board>().badeline)
                    {
                        badeline.SetActive(false);
                    }
                }
                board.ChangeLevel(actualBoard.cameraBoard, actualBoard.spotCam);
                actualBoard = board;
                
                if(GetComponent<Movement>().positions.Count > 0)
                    GetComponent<Movement>().positions.Clear();
                
                GetComponent<Movement>().positions.Enqueue(transform.position);
                
                if (actualBoard.GetComponent<Board>().containBadeline)
                {
                    
                    foreach (GameObject badeline in actualBoard.GetComponent<Board>().badeline)
                    {
                        badeline.SetActive(true);
                    }
                }
            }
        }
    }
}
