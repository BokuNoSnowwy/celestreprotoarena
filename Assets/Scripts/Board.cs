﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using JetBrains.Annotations;
using UnityEngine;

public class Board : MonoBehaviour
{
    public CinemachineVirtualCamera cameraBoard;
    public Transform spawnPlayer;
    [SerializeField] private GameObject _player;
    
    public Transform spotCam;

    public GameObject levelEnter;
    
    [Header("Badeline")]
    public bool containBadeline;
    public int numberBadeline;
    public  List<GameObject> badeline = new List<GameObject>();
    
    
    // Start is called before the first frame update
    void Start()
    { 
        _player = FindObjectOfType<Player>().gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeLevel([CanBeNull] CinemachineVirtualCamera oldCam, [CanBeNull] Transform spotCam)
    {
        cameraBoard.gameObject.SetActive(true);
        if (oldCam && oldCam != cameraBoard)
        {
            oldCam.gameObject.SetActive(false);
        }
        RefreshAllRespawnableObjects();
        RefreshAllObjects();
    }
    
    public void ChangeLevelLevelDesignTP([CanBeNull] CinemachineVirtualCamera oldCam, [CanBeNull] Transform spotCam)
    {
        cameraBoard.gameObject.SetActive(true);
        
        //Debug.Log(oldCam.ToString() +  cameraBoard);
        if (oldCam && oldCam != cameraBoard)
        {
            if (oldCam.Follow)
            {
                oldCam.Follow = null;
                oldCam.gameObject.SetActive(false);
                if (spotCam)
                {
                    oldCam.transform.position = spotCam.position;
                    StartCoroutine(differFollow(oldCam));
                }
            }
        }
        RefreshAllRespawnableObjects();
        RefreshAllObjects();
    }
    
    public IEnumerator differFollow(CinemachineVirtualCamera cam)
    {
        yield return new WaitForSeconds(1f);
        cam.Follow = _player.transform;
    }
    
    public void ChangeLevelFromLevelManager()
    {
        CinemachineVirtualCamera oldCam = _player.GetComponent<Player>().actualBoard.cameraBoard;
        Transform oldSpot = _player.GetComponent<Player>().actualBoard.spotCam;
        
        _player.transform.position = spawnPlayer.position;
        _player.GetComponent<Player>().actualBoard = this;
        ChangeLevelLevelDesignTP(oldCam,oldSpot);
    }

    public void RespawnPlayerHere()
    {
        if (_player)
        {
            _player.transform.position = spawnPlayer.position;
            _player.GetComponent<Movement>().positions.Clear();
            _player.GetComponent<Movement>().positions.Enqueue(_player.transform.position);
            ChangeLevel(cameraBoard,null);
        }
    }

    public void RefreshAllRespawnableObjects()
    {
        foreach (var element in GetComponentsInChildren<RespawnableElements>())
        {
            element.OnRespawn();
        }
    }

    public void RefreshAllObjects()
    {
        foreach (var dbloc in GetComponentsInChildren<DreamBlocs>())
        {
            dbloc.playerInBloc = false;
        }
    }    
}
