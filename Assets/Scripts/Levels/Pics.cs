﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public class Pics : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Rigidbody2D playerRb = other.gameObject.GetComponent<Rigidbody2D>();
            if (playerRb.velocity.y <=0)
            {
                other.GetComponent<Player>().Die();
            }
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        Rigidbody2D playerRb = other.gameObject.GetComponent<Rigidbody2D>();
        if (other.gameObject.CompareTag("Player"))
        {
            if (playerRb.velocity.y <=0)
            {
                other.GetComponent<Player>().Die();
            }
        }
    }
}
