﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ShieldBlocs : RespawnableElements
{
    public List<ShieldTrigger> triggerList = new List<ShieldTrigger>();
    public Transform movePos;
    private bool isActivated;
    
    // Start is called before the first frame update
    void Start()
    {
        respawnTransform = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void CheckAllTrigger()
    {
        int nbOfTrigger = 0;
        foreach (var trigger in triggerList)
        {
            if (trigger.isActivated)
            {
                nbOfTrigger += 1;
            }
        }

        if (nbOfTrigger == triggerList.Count)
        {
            if (!isActivated)
            {
                MoveBloc();
            }
        }
    }

    public override void OnRespawn()
    {
        transform.position = respawnTransform;
    }

    public void MoveBloc()
    {
        isActivated = true;
        transform.DOMove(movePos.position, 2f).SetEase(Ease.Linear);
    }
}
