﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEditor;
using UnityEngine;

public class SinkingPlatform : RespawnableElements
{
    public Transform basePoint;
    public bool goDown;
    public float speed;
    
    
    // Start is called before the first frame update
    void Start()
    {
        //basePoint = transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (goDown)
        {
            transform.Translate(Vector3.down * Time.deltaTime * speed);
        }
        else
        {
            transform.Translate(Vector3.up * Time.deltaTime * speed);

            if (transform.position.y > basePoint.position.y)
            {
                transform.position = basePoint.position;
            }
            //transform.DOMove(basePoint.position, 1f * Vector3.Distance(transform.position, basePoint.position));
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            
            goDown = true;
        }
    }
    
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            goDown = false;
        }
    }

    public override void OnRespawn()
    {
        transform.position = basePoint.transform.position;
    }
}
