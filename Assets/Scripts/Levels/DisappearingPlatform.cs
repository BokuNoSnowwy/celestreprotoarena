﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class DisappearingPlatform : MonoBehaviour
{
    [SerializeField] private Movement playerMov;
    public BoxCollider2D hisCollider;
    public SpriteRenderer hisSprite;
    [SerializeField] private float timerAfterDisapear;
    [SerializeField] private float timerBeforeDisapear;

    [SerializeField] private bool isOn;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isOn)
        {
            if (timerBeforeDisapear >= 0)
            {
                timerBeforeDisapear -= Time.deltaTime;
            }

            if (timerBeforeDisapear <= 0)
            {
                hisCollider.enabled = false;
                hisSprite.enabled = false;
                timerAfterDisapear = 3;
                isOn = false;
            }
        }
        if (timerAfterDisapear >= 0)
        {
            timerAfterDisapear -= Time.deltaTime;
        }

        if (timerAfterDisapear <= 0 && hisCollider.enabled == false)
        {
            hisCollider.enabled = true;
            hisSprite.enabled = true;
        }
    }


    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerMov = other.gameObject.GetComponent<Movement>();
            playerMov.hasDashed = false;
            timerBeforeDisapear = 2;
            isOn = true;
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            isOn = false;

        }
    }
}
