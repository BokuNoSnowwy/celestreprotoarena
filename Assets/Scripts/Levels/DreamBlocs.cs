﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DreamBlocs : MonoBehaviour
{
    public GameObject player;
    private Movement playerMovement;
    public bool playerInBloc;
    public bool canBeKilled;
    [SerializeField] private Vector2 _velocityPlayer;

    private BoxCollider2D _collider2D;


    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>().gameObject;
        _collider2D = GetComponent<BoxCollider2D>();
        playerMovement = player.GetComponent<Movement>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (playerMovement.isDashing)
        {
            gameObject.layer = LayerMask.NameToLayer("Default");
            _collider2D.isTrigger = true;

        }
        else if(!playerMovement.isDashing && !playerInBloc)
        {
            gameObject.layer = LayerMask.NameToLayer("Ground");
            _collider2D.isTrigger = false;
        }

        if (playerInBloc)
        {
            if (_velocityPlayer != Vector2.zero)
            {
                player.GetComponent<Rigidbody2D>().velocity = _velocityPlayer;
                //player.GetComponent<Movement>().isDashing = true;
            }
            
            Collision collisionPlayer = player.GetComponent<Collision>();
            if (collisionPlayer.onWall /*&& Mathf.Abs(player.GetComponent<Rigidbody2D>().velocity.x) < 0.5f*/)
            {
                //player.GetComponent<Player>().Die();   
            }
            
            if (collisionPlayer.onGround /*&& Math.Abs(player.GetComponent<Rigidbody2D>().velocity.y) < 0.5f*/)
            {
                //player.GetComponent<Player>().Die();   
            }
        }
        else
        {
            //player.GetComponent<Movement>().isDashing = false;
        }
        
    }

    private void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            player.GetComponent<Rigidbody2D>().gravityScale = 0;
            playerInBloc = true;
            _velocityPlayer = player.GetComponent<Rigidbody2D>().velocity;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        playerMovement.hasDashed = false;
        player.GetComponent<Rigidbody2D>().gravityScale = 3;
        _velocityPlayer = Vector2.zero;
        playerInBloc = false;
    }
}
