﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldTrigger : RespawnableElements
{
    public bool isActivated;
    public ShieldBlocs shieldBloc;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isActivated)
        {
            GetComponent<SpriteRenderer>().color = Color.black;
        }
        else
        {
            GetComponent<SpriteRenderer>().color = Color.white;
        }
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            isActivated = true;
            shieldBloc.CheckAllTrigger();
        }
    }

    public override void OnRespawn()
    {
        isActivated = false;
    }
}
