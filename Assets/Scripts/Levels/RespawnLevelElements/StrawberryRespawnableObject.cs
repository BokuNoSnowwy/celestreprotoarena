﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrawberryRespawnableObject : RespawnableElements
{
    public bool onPlayer;

    private GameObject _player;

     [SerializeField] private bool isValidated;
    // Start is called before the first frame update
    void Start()
    {
        respawnTransform = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (onPlayer)
        {
            Vector3 offsetPos = new Vector3(_player.transform.position.x,_player.transform.position.y+0.5f,0f);
            transform.position =  offsetPos;

            if (_player.GetComponent<Movement>().groundTouch)
            {
                Valitated();
            }
        }
        if (isValidated)
        {
            GetComponent<SpriteRenderer>().color = Color.black;
        }
        else
        {
            GetComponent<SpriteRenderer>().color = Color.white;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            _player = other.gameObject;
            onPlayer = true;
        }
    }

    public void Valitated()
    {
        isValidated = true;
        onPlayer = false;
        GetComponent<Collider2D>().enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;

    }

    public override void OnRespawn()
    {
        Debug.Log("Respawn Element : " + gameObject.name);
        transform.position = respawnTransform;
        onPlayer = false;
        GetComponent<Collider2D>().enabled = true;
        GetComponent<SpriteRenderer>().enabled = true;
    }
}

