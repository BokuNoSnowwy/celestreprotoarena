﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public GameObject panelLevelSelection;
    //public List<Board> LevelBoards = new List<Board>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            DisplayLevelSelection();
        }
    }

    public void DisplayLevelSelection()
    {
        if (!panelLevelSelection.activeSelf)
        {
            panelLevelSelection.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
            panelLevelSelection.SetActive(false);
        }
    }

    public void ResartGame()
    {
        SceneManager.LoadScene(0);
    }

    public void ChangeLevel(Board board)
    {
        DisplayLevelSelection();
        board.ChangeLevelFromLevelManager();
    }
}
