﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class LinearSludge : RespawnableElements
{
    public bool goRight, goUp;

    public bool horizontal;
    public float speed = 3f;
    public float distance = 0.5f;

    public Transform pointA, pointB;
    public float timeToTravel;

    private Sequence movementSequence;

    void Start()
    {
        transform.position = pointA.position;
    }
    
    void Update()
    {
        if (horizontal)
        {
            if (goRight)
            {
                transform.position += Vector3.right * Time.deltaTime * speed;
                if (Vector3.Distance(transform.position, pointB.position) <= distance)
                {
                    goRight = false;
                }
            }
            else
            {
                transform.position += Vector3.left * Time.deltaTime * speed;
                if (Vector3.Distance(transform.position, pointA.position) <= distance)
                {
                    goRight = true;
                }
            }
        }
        else
        {
            if (goUp)
            {
                transform.position += Vector3.up * Time.deltaTime * speed;
                if (Vector3.Distance(transform.position, pointB.position) <= distance)
                {
                    goUp = false;
                }
            }
            else
            {
                transform.position += Vector3.down * Time.deltaTime * speed;
                if (Vector3.Distance(transform.position, pointA.position) <= distance)
                {
                    goUp = true;
                }
            }
        }
        
    }
    
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<Player>().Die();
        }
    }
    
    public override void OnRespawn()
    {
        transform.position = pointA.position;
    }
}
