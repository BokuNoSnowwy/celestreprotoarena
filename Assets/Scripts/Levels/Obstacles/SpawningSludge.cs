﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class SpawningSludge : RespawnableElements
{
    public bool sludgeIsSpawned;
    public Sprite notSpawnedSludge, spawnedSludge;

    public void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player") && sludgeIsSpawned)
        {
            other.gameObject.GetComponent<Player>().Die();
            Debug.Log("Death");
        }
    }
    
    
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            sludgeIsSpawned = true;

            GetComponent<SpriteRenderer>().sprite = spawnedSludge;
        }
    }
    
    public override void OnRespawn()
    {
        GetComponent<Collider2D>().enabled = false;
        sludgeIsSpawned = false;
        GetComponent<SpriteRenderer>().sprite = notSpawnedSludge;
        GetComponent<Collider2D>().enabled = true;
    }
}
