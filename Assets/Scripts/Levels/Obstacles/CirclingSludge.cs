﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CirclingSludge : RespawnableElements
{
    public float speed;
    public Transform pointToRotateAround;
    private Transform _respawnPoint;

    void Start()
    {
        _respawnPoint = transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(pointToRotateAround.position, Vector3.forward, Time.deltaTime * speed);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<Player>().Die();
        }
    }

    public override void OnRespawn()
    {
        transform.position = _respawnPoint.position;
    }
}
