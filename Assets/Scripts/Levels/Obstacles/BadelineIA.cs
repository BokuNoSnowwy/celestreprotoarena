﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class BadelineIA : RespawnableElements
{
    public Vector3 destination;
    public GameObject player;
    public float speed;
    public float distance;

    public float startTimer;
    private bool _started;

    public Transform spawnPoint;

    void Start()
    {
        startTimer = 1f;
    }

    
    void Update()
    {
        startTimer -= Time.deltaTime;
        
        

        if (startTimer <= 0)
        {
            _started = true;
           // destination = player.GetComponent<Movement>().positions.Dequeue();
        }

        if (_started)
        {
            transform.position = Vector2.MoveTowards(transform.position, destination, speed * Time.deltaTime);
        
            if (Vector2.Distance(transform.position, destination) < distance)
            {
                player.GetComponent<Movement>().positions.Dequeue();
                
                
                if (player.GetComponent<Movement>().positions.Count <= 0)
                {
                    destination = player.transform.position;
                }
                else
                {
                    destination = player.GetComponent<Movement>().positions.Dequeue();
                }
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<Player>().Die();
            Debug.Log("Death");
        }
    }

    public void MajDestination()
    {
        destination = player.GetComponent<Movement>().positions.Peek();
    }
    
    
    public override void OnRespawn()
    {
        transform.position = spawnPoint.position;
        startTimer = 1f;
        _started = false;
    }
}
