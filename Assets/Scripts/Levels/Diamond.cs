﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Diamond : MonoBehaviour
{
    [SerializeField] private Movement playerMov;
    public CircleCollider2D hisCollider;
    public SpriteRenderer hisSprite;
    [SerializeField] private float timer;
// Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (timer >= 0)
        {
            timer -= Time.deltaTime;
        }

        if (timer <= 0 && hisCollider.enabled == false)
        {
            hisCollider.enabled = true;
            hisSprite.enabled = true;
        }
    }


    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerMov = other.gameObject.GetComponent<Movement>();
            if (playerMov.hasDashed)
            {
                hisCollider.enabled = false;
                playerMov.hasDashed = false;
                hisSprite.enabled = false;
                timer = 2;
            }
      
        }
    }
}
